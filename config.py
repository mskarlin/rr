import os
# Flask config.py

SQLALCHEMY_DATABASE_URI = "postgresql:///RRdb2"
MAX_CONTENT_LENGTH = 16 * 1024 * 1024 # 16 mb maximum upload
UPLOADED_FILES_ALLOW = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
SECRET_KEY='\x7f\x891\xab\xcb\xb1c\xbb*\xfb<\xae\x0f\xecb\xb7\xc7\xc7\xb9\xbe{\x16\xc6\xc2'
BCRYPT_LOG_ROUNDS = 12 # number of password hashes
basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
UPLOAD_FOLDER = '/home/mike/Development/RentRebel/app/static/dummy_data'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])


#Mailing config
# email server
MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 465
MAIL_USE_TLS = False
MAIL_USE_SSL = True
MAIL_USERNAME=''
MAIL_PASSWORD = ''
MAIL_SUPPRESS_SEND =  False

# administrator list
ADMINS = ['michael.skarlinski@gmail.com']
