# ourapp/models.py

from sqlalchemy.ext.hybrid import hybrid_property

import pandas as pd

from app import bcrypt, db

import datetime

class User(db.Model):
    '''
    User db object
    '''
    # standard account info
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(128), unique=True)
    utype=db.Column(db.Integer, default=0)
    #email_confirmed = db.Column(db.Integer, default=0)
    is_active=db.Column(db.Boolean,default=0)
    _password = db.Column(db.String(128))
    authenticated = db.Column(db.Boolean, default=False)

    @hybrid_property
    def password(self):
        return self._password

    @password.setter
    def _set_password(self, plaintext):
        self._password = bcrypt.generate_password_hash(plaintext)

    def is_correct_password(self, plaintext):
        return bcrypt.check_password_hash(self._password, plaintext)

    def is_authenticated(self):
        return self.authenticated
     #return True

    def is_active(self):
        return self.is_active
 
    def is_anonymous(self):
        return False
 
    def get_id(self):
        return unicode(self.id)


    # Personal Accnt info
    name = db.Column(db.String(128),default='Ned Stark')
    about_me = db.Column(db.Text,default='')
    pics=db.relationship('pic_ids',backref='user',lazy='dynamic')
    owned_property=db.relationship('Rentals',backref='landlord',lazy='dynamic',foreign_keys="Rentals.landlord_id")
    renting_property=db.relationship('Rentals',backref='tenant',lazy='dynamic',foreign_keys="Rentals.tenant_id")
    #action feed
    action=db.relationship('action_feed',backref='user',lazy='dynamic')
    #messages
    sent_messages=db.relationship('messages',backref='sender',lazy='dynamic',foreign_keys="messages.sender_id")
    recieved_messages=db.relationship('messages',backref='reciever',lazy='dynamic',foreign_keys="messages.reciever_id")
    proposed_events=db.relationship('scheduling',backref='scheduler',lazy='dynamic',foreign_keys="scheduling.scheduled_by")
    recieved_events=db.relationship('scheduling',backref='responder',lazy='dynamic',foreign_keys="scheduling.referring_to")
    


#really only need one to many relationship
#each instantiation of this class represents ONE landlord to tenant relationship
class Rentals(db.Model):
    '''
    Rental unit DB object
    '''
    # unique property ID
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    #geographic info for property
    address = db.Column(db.String(128))
    ptype = db.Column(db.String(64),default="House or Condo    ") #apt, room, house/condo
    pet_status = db.Column(db.String(64),default="no_pets") #apt, room, house/condo
    brnum = db.Column(db.Float,default=1)
    apt_num = db.Column(db.String(64),default='NA')
    bathrnum = db.Column(db.Float,default=1.)
    #zipcode = db.Column(db.Integer)
    #city = db.Column(db.String(128))
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    # Historic info for propertyp
    # Current rental start date, then finish date, or lack therof
    StartDate = db.Column(db.DateTime,default=datetime.date(2016,10,2))
    FinishDate = db.Column(db.DateTime,default=datetime.date(2012,10,2))
    AvailableDate = db.Column(db.DateTime,default=datetime.date(2012,10,2)) # when the lease will be over/people can move in
    #rental info    
    rent=db.Column(db.Float,default=0.)
    utilities_estimate=db.Column(db.Float,default=0.)
    likes=db.Column(db.Integer,default=0)
    dislikes=db.Column(db.Integer,default=0)
    description=db.Column(db.Text, default='')
    landlord_id=db.Column(db.Integer, db.ForeignKey('user.id'))    
    tenant_id=db.Column(db.Integer,db.ForeignKey('user.id'))
    #picture IDs
    pics=db.relationship('pic_ids',backref='rental',lazy='dynamic')
    #tags
    tags=db.relationship('tags',backref='rental',lazy='dynamic')
    #action_events
    action=db.relationship('action_feed',backref='rental',lazy='dynamic')
    #messages    
    messages=db.relationship('messages',backref='rental',lazy='dynamic')
    #scheduling
    schedules=db.relationship('scheduling',backref='rental',lazy='dynamic')


class tags(db.Model):
    '''
    property tags objects
    '''
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    tag = db.Column(db.String(64),default='')
    property_id=db.Column(db.Integer,db.ForeignKey('rentals.id'))

acat_list=['register', 'login', 'logout', 'sent_mess', 'rec_mess', 'new_prop', 'like_prop', 'dislike_prop', 'comment_prop', 'contact_prop', 'edit_prop']

class action_feed(db.Model):
    '''
    Database  to store  all  user actions with current time and type of action
    
    catagories: register, login, logout, sent_mess, rec_mess, new_prop, like_prop, dislike_prop, comment_prop, contact_prop, edit_prop
    
    '''
    id=db.Column(db.Integer, primary_key=True, autoincrement=True)
    action_type=db.Column(db.String(128),default='')
    action_time=db.Column(db.DateTime,default=datetime.datetime.utcnow())
    property_id=db.Column(db.Integer,db.ForeignKey('rentals.id'))
    user_id=db.Column(db.Integer,db.ForeignKey('user.id'))
        

class messages(db.Model):
    '''
    message objects for each message
    '''
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    subject = db.Column(db.String(64),default='')
    sender_id=db.Column(db.Integer,db.ForeignKey('user.id'))
    reciever_id=db.Column(db.Integer,db.ForeignKey('user.id'))
    message=db.Column(db.Text, default='')    
    message_time=db.Column(db.DateTime,default=datetime.datetime.utcnow())    
    property_id=db.Column(db.Integer,db.ForeignKey('rentals.id'))

class pic_ids(db.Model):
    '''
    picture decscriptors for each picture
    '''
    #unique picture name which will be associated to the Rentals
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name=db.Column(db.Text)
    property_id=db.Column(db.Integer,db.ForeignKey('rentals.id'))
    user_id=db.Column(db.Integer,db.ForeignKey('user.id'))


#types include unaccepted, accepted, accepted_remind.
class scheduling(db.Model):
    '''
    scheduling objects for meeting times
    '''
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    date=db.Column(db.DateTime,default=datetime.datetime.utcnow())
    time=db.Column(db.String(128),default='')
    schedule_type=db.Column(db.String(128),default='')
    scheduled_by=db.Column(db.Integer,db.ForeignKey('user.id'))
    referring_to=db.Column(db.Integer,db.ForeignKey('user.id'))
    property_id=db.Column(db.Integer,db.ForeignKey('rentals.id'))


