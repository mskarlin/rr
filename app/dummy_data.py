#here we populate the DB with some dummy data and pictures.
from app import db
from models import *
import pandas as pd
from numpy.random import randint
import numpy as np

from geopy.geocoders import GoogleV3
geolocator = GoogleV3()



def add_dummy_data(db):

	df_users=pd.read_csv('./app/static/dummy_data/dummy_people.csv')
	df_props=pd.read_csv('./app/static/dummy_data/dummy_table.csv')
	df_tags=pd.read_csv('./app/static/dummy_data/initial_tags.csv')	


	for u in df_users.index:

	    user = User(email = df_users.email[u],
		    password = df_users.password[u],
		    utype = df_users.utype[u],
		    name = df_users.name[u])

	    db.session.add(user)
	db.session.commit()
	
	u=User.query.all()
		
	# remember there is a property for EACH relationship
	# say the first two are in the same house, same landlord
	for p in df_props.index[:2]:
	    location = geolocator.geocode(df_props.address[0]+' '+df_props.city[0])    
            prop = Rentals(address = df_props.address[0],
			   latitude = location.latitude,
			   longitude = location.longitude,
			   StartDate = df_props.StartDate[0],
			   FinishDate = df_props.FinishDate[0],
		           AvailableDate=df_props.AnticipatedDate[0],
			   rent = df_props.rent[0],
			   utilities_estimate=df_props.utilities_estimate[0],
			   landlord=u[6],
			   tenant=u[p])
	    db.session.add(prop)


	for p in df_props.index[2:6]:
	    location = geolocator.geocode(df_props.address[p]+' '+df_props.city[p])    
            prop = Rentals(address = df_props.address[p],
			   latitude = location.latitude,
			   longitude = location.longitude,
			   StartDate = df_props.StartDate[p],
			   FinishDate = df_props.FinishDate[p],
		           AvailableDate=df_props.AnticipatedDate[p],
			   rent = df_props.rent[p],
			   utilities_estimate=df_props.utilities_estimate[p],
			   landlord=u[7],
			   tenant=u[p])
	    db.session.add(prop)

	for p in df_props.index[6:]:
	    location = geolocator.geocode(df_props.address[p]+' '+df_props.city[p])    
            prop = Rentals(address = df_props.address[p],
			   latitude = location.latitude,
			   longitude = location.longitude,
			   StartDate = df_props.StartDate[p],
			   FinishDate = df_props.FinishDate[p],
		           AvailableDate=df_props.AnticipatedDate[p],
			   rent = df_props.rent[p],
			   utilities_estimate=df_props.utilities_estimate[p],
			   landlord=u[8])
	    db.session.add(prop)
	
	db.session.commit()
	r=Rentals.query.all()
	for p in df_props.index:
		pid=pic_ids(rental=r[p])
		db.session.commit()
		pid.name=str(pid.id)+'.jpg' 
		db.session.add(pid)
	db.session.commit()
	
	for p in df_props.index:
		np.random.shuffle(df_tags.tags)
		for tag in df_tags.tags.head(np.random.randint(0,len(df_tags))):
			t=tags(rental=r[p],tag=tag)
			db.session.add(t)
	db.session.commit()

