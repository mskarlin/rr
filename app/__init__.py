from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.mail import Mail
from flask.ext.bcrypt import Bcrypt
from itsdangerous import URLSafeTimedSerializer
from flask.ext.login import LoginManager


app= Flask(__name__, instance_relative_config=True)

app.config.from_object('config')
#app.config.from_pyfile('config.py')

#database and mail applications from flask
db = SQLAlchemy(app)
mail = Mail(app)
bcrypt=Bcrypt(app)
ts = URLSafeTimedSerializer(app.config["SECRET_KEY"])

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view =  "marketplace"

from models import User
# allows us to check for logged in users
@login_manager.user_loader
def load_user(userid):
    return User.query.filter(User.id==userid).first()

from views import *
#db.reflect()
#db.drop_all()
db.create_all()
