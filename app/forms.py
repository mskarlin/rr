#next line is wrong but may be correct after reformatting.
# ourapp/forms.py
from flask.ext.wtf import Form
from wtforms.fields import TextField, PasswordField
from wtforms.validators import Required, Email
from validators import Unique
from models import User

# this is a class for email/password form whiich requires validators to stop Cross site request forgery
class EmailPasswordForm(Form):
    email = TextField('Email', validators=[Required(), Email()])
    password = PasswordField('Password', validators=[Required()])


class RegistrationForm(Form):
    name = TextField('name', validators=[Required()])
    email = TextField('Email', validators=[Required(), Email(),Unique(
            User,
            User.email,
            message='There is already an account with that email.')])
    password = PasswordField('Password', validators=[Required()])
    utype = TextField('User_Type', validators=[Required()])

class PropertyForm(Form):
    address = TextField('address', validators=[Required()])
    apt_num = TextField('apt_num', validators=[Required()])
    #city = TextField('city', validators=[Required()])
    #zipcode = TextField('zipcode', validators=[Required()])
    ptype = TextField('ptype', validators=[Required()])
    date_avail=TextField('date_avail',validators=[Required()])
    brnum = TextField('brnum', validators=[Required()])
    bathrnum = TextField('bathrnum', validators=[Required()])
    pet_status = TextField('pet_status', validators=[Required()])
    rent = TextField('rent',validators=[Required()])
    description = TextField('description',validators=[Required()])

