from flask import render_template,request,redirect,url_for,jsonify,flash
from flask.ext.login import login_required, current_user
from flask_bootstrap import Bootstrap
from sqlalchemy.sql.expression import or_, and_
from flask_mail import Message
from map_functions import *
from app import *
from forms import *
import os
from models import *
from dummy_data import *
from flask.ext.login import login_user, logout_user
from geopy.geocoders import GoogleV3
geolocator = GoogleV3()

from werkzeug import secure_filename

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']


@app.context_processor
def utility_processor():
    '''
    context processor for functions to be executed within the jinja2 template
    '''
    def make_int(curr):
        return u'{0:.0f}'.format(int(curr))
    def truncate_first(curr):
        return str(curr).rsplit(' ', 1)[0]
    def truncate_comma(curr):
        return str(curr).rsplit(',')[0]
    def view_dates(curr):
        dn=str(curr).rsplit(' ')[0].rsplit('-')
        return str(dn[1]+'/'+dn[2]+'/'+dn[0])
    return dict(make_int=make_int,truncate_first=truncate_first,truncate_comma=truncate_comma,view_dates=view_dates)

 


class map_state():
    '''
    lat,long centered on rochester, used for testing purposes,
    ideally this will be gathered from browser info
    '''
    center=[43.141266, -77.60440075]


ms=map_state()


@app.route('/marketplace', methods=["GET", "POST"])
def marketplace():
    form = RegistrationForm()
    form2 = EmailPasswordForm()
    form_prop=PropertyForm()
    #add_dummy_data(db)
    local_tags=['Cool cats','Big doors', 'roof patio']

    r=models.Rentals.query.all()
    if len(request.args.getlist('center')) > 0:
        ms.center=[float(request.args.getlist('center')[0]),float(request.args.getlist('center')[1])]
    
    if current_user.is_active:
        lb = 'Account Info'
        lint=False    
    else:
        lb= 'Click here to Login/Register'
        lint=True

    if request.method=="POST":            

        if request.form.get('search_butt',None)=='Search':
            #import pdb; pdb.set_trace()
            temploc=get_latlong(request.form['search_loc'])
            if len(temploc)==0:
                # error message should go here
                return redirect('/marketplace')
        
            return redirect(url_for('marketplace', center=temploc, lint=False))

        elif request.form.get('logout',None)=='Logout': 
            #import pdb; pdb.set_trace()
            return redirect('/logout')

        elif request.form.get('profile',None)=='My Profile': 
            #import pdb; pdb.set_trace()
            return redirect('/my_account')

        elif form2.validate_on_submit() and request.form.get('user_login',None)=='Login':
            #import pdb; pdb.set_trace()
            user = models.User.query.filter_by(email=form2.email.data).first_or_404()
            if user.is_correct_password(form2.password.data):
                login_user(user)
                user.is_active=True
                db.session.commit()

        elif form.validate_on_submit() and request.form.get('user_login',None)=="Register":
            user = User(email = form.email.data,
                password = form.password.data,
                utype = form.utype.data,
                name = form.name.data,
                about_me=request.form.get('about_me',None))
            
            db.session.add(user)
            db.session.commit()

            a=action_feed(action_type='register',user=user)
            db.session.add(a)
            db.session.commit()
            
            #add profile picture
            try:file = request.files['profile_image']
            except:file=None
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                pid=pic_ids(user=user) 
                db.session.add(pid)
                db.session.commit()
                pid.name=str(pid.id)+'.'+ str(filename.rsplit('.', 1)[1])
                db.session.commit()
                file.save(os.path.join(app.config['UPLOAD_FOLDER'],str(pid.id)+'.'+ str(filename.rsplit('.', 1)[1])))

            # Now we'll send the email confirmation link
            #subject = "Confirm your email"
            
            token = ts.dumps(user.email, salt='email-confirm-key')

            confirm_url = url_for('confirm_email',token=token,_external=True)

            html = render_template(
                    'email/activate.html',
                    confirm_url=confirm_url)

            #send_email(user.email, subject, html)
            msg = Message(sender="michael.skarlinski@gmail.com",recipients=  ["michael.skarlinski@gmail.com"])        
            msg.html=html
            mail.send(msg)
        
        #remember to add validation here!
        elif request.form.get("submit_prop",None):

            location = geolocator.geocode(form_prop.address.data)            
            if current_user.utype==0:
                ten=current_user
                ll=None
            else: 
                ll=current_user
                ten=None
            prop = Rentals(address = form_prop.address.data,
               latitude = location.latitude,
               longitude = location.longitude,
               ptype= form_prop.ptype.data,
               AvailableDate=form_prop.date_avail.data,
               apt_num=form_prop.apt_num.data,
               rent = form_prop.rent.data,
               description=form_prop.description.data,
               brnum=form_prop.brnum.data,
               bathrnum=form_prop.bathrnum.data,
               pet_status=form_prop.pet_status.data,
               tenant=ten,
               landlord=ll)    

            db.session.add(prop)
            db.session.commit()
            #import pdb; pdb.set_trace()            
            for x in range(1,8):            
                try:file = request.files['photo'+str(x)]
                except:file=None
                if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    pid=pic_ids(rental=prop) 
                    db.session.add(pid)
                    db.session.commit()
                    pid.name=str(pid.id)+'.'+ str(filename.rsplit('.', 1)[1])
                    db.session.commit()
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'],str(pid.id)+'.'+ str(filename.rsplit('.', 1)[1])))
            
            for t in local_tags:
                if request.form.get(t,None)==t:
                    tag=tags(rental=prop,tag=t)
                    db.session.add(tag)
            db.session.commit()
            for t in request.form.get("usertags",None).rsplit('#'):
                tag=tags(rental=prop,tag=t)
                db.session.add(tag)    
            db.session.commit()
    
            a=action_feed(action_type='new_prop',user=current_user)
            db.session.add(a)    
            db.session.commit()
        elif request.form.get("sent_threads",None):
            return redirect(url_for('show_property',prop_id=request.form.get("sent_threads",None)))
        #elif request.form.get("property_profile",None):
            #import pdb; pdb.set_trace()
            #prop = models.Rentals.query.filter_by(id=request.form.get("property_profile",None)).first_or_404()
            #return redirect('show_property/'+str(prop.id))
        return redirect("/marketplace")


    return render_template('index_mod.html',form=form,form2=form2,form2_status=lb,rental=r,ms=ms,login_intro=lint,form_prop=form_prop, tags=local_tags)


@app.route("/logout")
def logout():
    user=current_user
    user.is_active=False 
    db.session.commit()
    logout_user()
    return redirect('/marketplace')


@app.route('/show_property', methods=["GET", "POST"])
def show_property():
    # get property for the template
    #import pdb; pdb.set_trace()
    if request.args.get('prop_id'):
        this_prop = models.Rentals.query.filter_by(id=request.args.get('prop_id')).first_or_404()
        # need to make the dates specific to this property
        this_date=models.scheduling.query.filter(and_(scheduling.scheduled_by==this_prop.tenant.id,scheduling.property_id==this_prop.id)) 
    else:
        this_prop=None
        this_date=None
        
    form = RegistrationForm()
    form2 = EmailPasswordForm()
    form_prop=PropertyForm()
    #add_dummy_data(db)
    local_tags=['Cool cats','Big doors', 'roof patio']
    
    r=models.Rentals.query.all()
    
    if len(request.args.getlist('center')) > 0:
        ms.center=[float(request.args.getlist('center')[0]),float(request.args.getlist('center')[1])]
    
    if current_user.is_active:
        lb = 'Account Info'
        lint=False    
    else:
        lb= 'Click here to Login/Register'
        lint=True

    if request.method=="POST":            

        if request.form.get('search_butt',None)=='Search':
            #import pdb; pdb.set_trace()
            temploc=get_latlong(request.form['search_loc'])
            if len(temploc)==0:
                # error message should go here
                return redirect('/show_property')
        
            return redirect(url_for('show_property', center=temploc, lint=False))

        elif request.form.get('logout',None)=='Logout': 
            #import pdb; pdb.set_trace()
            return redirect('/logout')
        
        elif request.form.get('profile',None)=='My Profile': 
            #import pdb; pdb.set_trace()
            return redirect('/logout')


        elif form2.validate_on_submit() and request.form.get('user_login',None)=='Login':
            #import pdb; pdb.set_trace()
            user = models.User.query.filter_by(email=form2.email.data).first_or_404()
            if user.is_correct_password(form2.password.data):
                login_user(user)
                user.is_active=True
                db.session.commit()

        elif form.validate_on_submit() and request.form.get('user_login',None)=="Register":
            user = User(email = form.email.data,
                password = form.password.data,
                utype = form.utype.data,
                name = form.name.data)
            db.session.add(user)
            db.session.commit()
            # Now we'll send the email confirmation link
            #subject = "Confirm your email"
        
            token = ts.dumps(user.email, salt='email-confirm-key')

            confirm_url = url_for('confirm_email',token=token,_external=True)

            html = render_template(
                    'email/activate.html',
                    confirm_url=confirm_url)

            #send_email(user.email, subject, html)
            msg = Message(sender="michael.skarlinski@gmail.com",recipients=  ["michael.skarlinski@gmail.com"])        
            msg.html=html
            mail.send(msg)
        
        #remember to add validation here!
        elif request.form.get("submit_prop",None)=="Submit":

            location = geolocator.geocode(form_prop.address.data)            
            if current_user.utype==0:
                ten=current_user
                ll=None
            else: 
                ll=current_user
                ten=None
            prop = Rentals(address = form_prop.address.data,
               latitude = location.latitude,
               longitude = location.longitude,
               ptype= form_prop.ptype.data,
               AvailableDate=form_prop.date_avail.data,
               apt_num=form_prop.apt_num.data,
               rent = form_prop.rent.data,
               description=form_prop.description.data,
               brnum=form_prop.brnum.data,
               bathrnum=form_prop.bathrnum.data,
               pet_status=form_prop.pet_status.data,
               tenant=ten,
               landlord=ll)    

            db.session.add(prop)
            db.session.commit()
            #import pdb; pdb.set_trace()            
            for x in range(1,9):            
                try:file = request.files['photo'+str(x)]
                except:file=None
                if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    pid=pic_ids(rental=prop) 
                    db.session.add(pid)
                    db.session.commit()
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'],str(pid.id)+'.'+ str(filename.rsplit('.', 1)[1])))
            
            for t in local_tags:
                if request.form.get(t,None)==t:
                    tag=tags(rental=prop,tag=t)
                    db.session.add(tag)
            db.session.commit()
            for t in request.form.get("usertags",None).rsplit('#'):
                tag=tags(rental=prop,tag=t)
                db.session.add(tag)
            db.session.commit()
            
            a=action_feed(action_type='new_prop',user=current_user)
            db.session.add(a)
            db.session.commit()

        elif request.form.get("property_profile",None):
            #import pdb; pdb.set_trace()
            prop = models.Rentals.query.filter_by(id=request.form.get("property_profile",None)).first_or_404()
            return redirect(url_for('show_property',prop_id=prop.id))

        elif request.form.get("back_to_marketplace",None):
            return redirect('/marketplace')

        elif request.form.get('send_message',None):
            m=messages(subject=request.form.get('subject',None),
                  sender=current_user,
                  reciever=this_prop.tenant,
                  message=request.form.get('message',None),
                  rental=this_prop)
            db.session.add(m)
            db.session.commit()
            
            if request.form.get('sel_schedule',None):
                sid=models.scheduling.query.filter_by(id=request.form.get('sel_schedule',None))
                sid.schedule_type='accepted'
                db.session.commit()            

            #add possibly scheduling dates to the db
            for v in range(1,6):
                if this_prop.tenant:
                    rt=this_prop.tenant
                else:
                    rt=this_prop.landlord
                if request.form.get('view_avail'+str(v),None):
                    s=scheduling(date=request.form.get('view_avail'+str(v),None),
                             time=request.form.get('time'+str(v),None)+' '+request.form.get('ampm'+str(v),None),
                              schedule_type='unaccepted',scheduled_by=current_user.id,referring_to=rt.id,rental=this_prop)
                    db.session.add(s)
                else:
                    continue


            db.session.commit()
        return redirect(url_for("show_property",prop_id=this_prop.id))


    return render_template('show_property.html',form=form,form2=form2,login_status=lb,rental=r,ms=ms,login_intro=lint,form_prop=form_prop, tags=local_tags,this_prop=this_prop,this_date=this_date)

@app.route('/my_account', methods=["GET", "POST"])
def my_account():
    # get property for the template
    #import pdb; pdb.set_trace()
    this_user=current_user            
    form = RegistrationForm()
    form2 = EmailPasswordForm()
    form_prop=PropertyForm()
    #add_dummy_data(db)
    local_tags=['Cool cats','Big doors', 'roof patio']

    #make a unique list with contacted people in it
    uthreads=[]
    for mess in [models.messages.query.filter_by(sender=current_user),models.messages.query.filter_by(reciever=current_user)]:
        for m in mess:
            if m.sender.id not in [ut.sender.id for ut in uthreads] and m.sender.id not in [ut.reciever.id for ut in uthreads] and m.sender.id != current_user.id:
                uthreads.append(m)
            elif m.reciever.id not in [ut.sender.id for ut in uthreads] and m.reciever.id not in [ut.reciever.id for ut in uthreads] and m.reciever.id != current_user.id:
                uthreads.append(m)

        r=models.Rentals.query.all()
    if len(request.args.getlist('center')) > 0:
        ms.center=[float(request.args.getlist('center')[0]),float(request.args.getlist('center')[1])]
    
    if current_user.is_active:
        lb = 'Account Info'
        lint=False    
    else:
        lb= 'Click here to Login/Register'
        lint=True
    
    if request.method=="POST":            

        if request.form.get('search_butt',None)=='Search':
            #import pdb; pdb.set_trace()
            temploc=get_latlong(request.form['search_loc'])
            if len(temploc)==0:
                # error message should go here
                return redirect('/marketplace')
        
            return redirect(url_for('marketplace', center=temploc, lint=False))

        elif form_prop.validate_on_submit() and request.form.get('add_prop',None)=='List!': 
            #import pdb; pdb.set_trace()
            location = geolocator.geocode(form_prop.address.data+' '+form_prop.city.data)    
            prop = Rentals(address = form_prop.address.data,
               zipcode = form_prop.zipcode.data,
               city = form_prop.city.data,
               latitude = location.latitude,
               longitude = location.longitude,
               rent = form_prop.rent.data,
               description=form_prop.description.data,
               tenant=current_user)
                        
            db.session.add(prop)
            db.session.commit()
        
            for x in range(1,4):            
                file = request.files['photo'+str(x)]
                if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    pid=pic_ids(rental=prop) 
                    db.session.add(pid)
                    db.session.commit()
                    pid.name=str(pid.id)+'.'+ str(filename.rsplit('.', 1)[1])
                    db.session.commit()
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'],str(pid.id)+'.'+ str(filename.rsplit('.', 1)[1])))

        elif request.form.get('logout',None)=='Logout': 
            #import pdb; pdb.set_trace()
            return redirect('/logout')
        
        elif request.form.get('profile',None)=='My Profile': 
            #import pdb; pdb.set_trace()
            return redirect('/logout')


        elif form2.validate_on_submit() and request.form.get('user_login',None)=='Login':
            #import pdb; pdb.set_trace()
            user = models.User.query.filter_by(email=form2.email.data).first_or_404()
            if user.is_correct_password(form2.password.data):
                login_user(user)
                user.is_active=True
                db.session.commit()
        


        elif form.validate_on_submit() and request.form.get('user_login',None)=="Register":
            user = User(email = form.email.data,
                password = form.password.data,
                utype = form.utype.data,
                name = form.name.data)
            db.session.add(user)
            db.session.commit()
            # Now we'll send the email confirmation link
            #subject = "Confirm your email"
        
            token = ts.dumps(user.email, salt='email-confirm-key')

            confirm_url = url_for('confirm_email',token=token,_external=True)

            html = render_template(
                    'email/activate.html',
                    confirm_url=confirm_url)

            #send_email(user.email, subject, html)
            msg = Message(sender="michael.skarlinski@gmail.com",recipients=  ["michael.skarlinski@gmail.com"])        
            msg.html=html
            mail.send(msg)
        
        #remember to add validation here!
        elif request.form.get("submit_prop",None)=="Submit":

            location = geolocator.geocode(form_prop.address.data)            
            if current_user.utype==0:
                ten=current_user
                ll=None
            else: 
                ll=current_user
                ten=None
            prop = Rentals(address = form_prop.address.data,
               latitude = location.latitude,
               longitude = location.longitude,
               ptype= form_prop.ptype.data,
               AvailableDate=form_prop.date_avail.data,
               apt_num=form_prop.apt_num.data,
               rent = form_prop.rent.data,
               description=form_prop.description.data,
               brnum=form_prop.brnum.data,
               bathrnum=form_prop.bathrnum.data,
               pet_status=form_prop.pet_status.data,
               tenant=ten,
               landlord=ll)    

            db.session.add(prop)
            db.session.commit()
            for x in range(1,4):            
                file = request.files['photo'+str(x)]
                if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    pid=pic_ids(rental=prop) 
                    db.session.add(pid)
                    db.session.commit()
                    pid.name=str(pid.id)+'.'+ str(filename.rsplit('.', 1)[1])
                    db.session.commit()
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'],str(pid.id)+'.'+ str(filename.rsplit('.', 1)[1])))
            
            for t in local_tags:
                if request.form.get(t,None)==t:
                    tag=tags(rental=prop,tag=t)
                    db.session.add(tag)
            db.session.commit()
            for t in request.form.get("usertags",None).rsplit('#'):
                tag=tags(rental=prop,tag=t)
                db.session.add(tag)
            db.session.commit()

        elif request.form.get("property_profile",None):
            #import pdb; pdb.set_trace()
            prop = models.Rentals.query.filter_by(id=request.form.get("property_profile",None)).first_or_404()
            return redirect(url_for('my_account',prop_id=prop.id))

        elif request.form.get("back_to_marketplace",None):
            return redirect('/marketplace')    
        
        elif request.form.get('submit_prop_edit',None):

            prop = models.Rentals.query.filter_by(id=current_user.renting_property[0].id).first_or_404()
            
            if request.form.get('e_address',None):
                prop.address=request.form.get('e_address',None)
            if request.form.get('e_ptype',None):
                prop.ptype=request.form.get('e_ptype',None)
            if request.form.get('e_apt_num',None):
                prop.apt_num=request.form.get('e_apt_num',None)
            if request.form.get('e_date_avail',None):
                prop.AvailableDate=request.form.get('e_date_avail',None)
            if request.form.get('e_brnum',None):
                prop.brnum=request.form.get('e_brnum',None)
            if request.form.get('e_bathrnum',None):
                prop.bathrnum=request.form.get('e_bathrnum',None)
            if request.form.get('e_pet_status',None):
                prop.pet_status=request.form.get('e_pet_status',None)
            if request.form.get('e_rent',None):
                prop.rent=request.form.get('e_rent',None)
            if request.form.get('e_description',None):
                prop.description=request.form.get('e_description',None)
            #finalize the changes
            db.session.commit()
            #import pdb; pdb.set_trace()
            #now we replace any images that need  replacement
            for p in current_user.renting_property[0].pics:            
                try: file = request.files[str(p.name)]
                except: file=None
                if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'],str(p.id)+'.'+ str(filename.rsplit('.', 1)[1])))
            # and add new pics if they choose
            for x in range(1,4):            
                try: file = request.files['photoadd'+str(x)]
                except: file=None
                if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    pid=pic_ids(rental=current_user.renting_property[0]) 
                    db.session.add(pid)
                    db.session.commit()
                    pid.name=str(pid.id)+'.'+ str(filename.rsplit('.', 1)[1])
                    db.session.commit()
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'],str(pid.id)+'.'+ str(filename.rsplit('.', 1)[1])))

            # and we add/remove tags that the user wishes    
            for t in current_user.renting_property[0].tags:
                if not request.form.get(t,None):
                    db.session.delete(t)
            db.session.commit()

            for t in local_tags:
                if request.form.get(t,None)==t and t not in current_user.renting_property[0].tags:
                    tag=tags(rental=prop,tag=t)
                    db.session.add(tag)
            db.session.commit()
            
            for t in request.form.get("usertags",None).rsplit('#'):
                if t not in current_user.renting_property[0].tags:
                    tag=tags(rental=prop,tag=t)
                    db.session.add(tag)
            db.session.commit()

            a=action_feed(action_type='edit_prop',user=current_user)
            db.session.add(a)
            db.session.commit()

        elif request.form.get('view_listing',None):
            prop = current_user.renting_property[0]
            return redirect(url_for('show_property',prop_id=prop.id))
        
        elif request.form.get('sent_threads',None):
            #import pdb; pdb.set_trace()
            # get all threads between the two people
            pmess=models.messages.query.filter_by(id=request.form.get('sent_threads',None))[0]
            #find the one that's NOT the current user
            if pmess.sender_id==current_user.id:other=models.User.query.filter_by(id=pmess.reciever_id)[0]
            else: other=models.User.query.filter_by(id=pmess.sender_id)[0]
    
            thread_sent=models.messages.query.filter(or_(and_(messages.reciever_id==int(other.id),messages.sender_id==current_user.id),and_(messages.reciever_id==current_user.id,messages.sender_id==int(other.id))))
            
            if thread_sent[0].sender!=current_user:
                s_dates=models.scheduling.query.filter(and_(scheduling.scheduled_by==thread_sent[0].sender.id,scheduling.property_id==thread_sent[0].property_id))            
            else:
                s_dates=None

            return render_template('show_thread.html',form=form,form2=form2,login_status=lb,rental=r,ms=ms,login_intro=lint,form_prop=form_prop, tags=local_tags,this_user=this_user,this_thread=thread_sent,meeting_confirmed=False,s_dates=s_dates,uthreads=uthreads)

        elif request.form.get('send_message',None):    

            message=models.messages.query.filter_by(id=int(request.form.get('send_message',None))).first_or_404()
            
            if message.sender != current_user:
                to_user=message.sender
            else: to_user=message.reciever

            m=messages(sender=current_user,
                   reciever=to_user,
                   message=request.form.get('message',None),
                   rental=current_user.renting_property[0])
            db.session.add(m)
            db.session.commit()

        elif request.form.get('add_dates',None):
            #add possibly scheduling dates to the db
            for v in range(1,6):
                if request.form.get('view_avail'+str(v),None):
                    s=scheduling(date=request.form.get('view_avail'+str(v),None),
                             time=request.form.get('time'+str(v),None)+' '+request.form.get('ampm'+str(v),None),
                              schedule_type='unaccepted',scheduled_by=current_user.id,rental=current_user.renting_property[0])
                    db.session.add(s)
                else:
                    continue

            db.session.commit()
                
             
        return redirect("/my_account")


    return render_template('account_page.html',form=form,form2=form2,login_status=lb,rental=r,ms=ms,login_intro=lint,form_prop=form_prop, tags=local_tags,this_user=this_user,uthreads=uthreads)


@app.route('/confirm/<token>')
def confirm_email(token):
    try:
        email = ts.loads(token, salt="email-confirm-key", max_age=86400)
    except:
        abort(404)

    user = User.query.filter_by(email=email).first_or_404()

    user.authenticated=True

    db.session.commit()

    return redirect(url_for('marketplace'))

