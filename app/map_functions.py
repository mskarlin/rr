from geopy.geocoders import GoogleV3
geolocator = GoogleV3()

def get_latlong(place):
    '''
    uses  the google geolocator to get lat long coords from addresses
    '''
    location = geolocator.geocode(place)
    try:	
        return [location.latitude,location.longitude]    
    except:
        return []
