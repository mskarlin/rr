This is the skeleton of the RentRef App--it's a Flask App which is set up to run on a postgres DB.

Initial instructions to run the app:

1. Either run in the virtual environment provided (source venv/bin/activate) or pip install necessary
modules in the requirements.txt file.

2. Make modifications to the config file--
    -SQLALCHEMY_DATABASE_URI needs to be set equal to a valid SQL DB
    -UPLOAD_FOLDER needs to be where you want your user uploaded files to go
    -MAIL_USERNAME and MAIL_PASSWORD need to be set to mail accounts (in this case gmail)
        to send when users login (although you can just add accounts by scripting in new users)
    -ADMINS should be whatever person needs admin privilege but it's not really implemented.

3. At this point you should  be able to run the app by simply ./python run.py in the main directory, but it'll be
    relatively unpopulated.
    With the current configuration, go to http://0.0.0.0:8080/marketplace to view the app locally.

3.Now since you're going to be pointing to a new DB, you need to populate it with some 'dummy data'.
    this is weaksauce, but the dummy data can be populated by uncommenting the 'add_dummy_data(db)' function
    (views.py at the top of the marketplace function). Then after running this once, you can comment it out for
    future runs since it's committed to the DB. To clear the DB at any time just uncomment #db.reflect() and
    #db.drop_all() in the __init__.py file, where the app is initialized as a whole.


Now the HTML looks really bad--and it's not responsive the way the app is now, the trick is adapting the flask functions
in view.py and modifying the jinja2 templating functions in the new pretty HTML pages we've provided to provide the same
functionality. The page currently enters via the marketplace function (index_mod.html template) but we want it to be a landing
page now.

Some nuts and bolts:

-run.py starts the whole program, it's currently be in debug mode by default.

-models.py has all of the classes which populate the DB, they are referenced heavily
    in the rest of the backend, as you would expect.

-views.py has pretty much all of the logic of the app, each function has a different page which handles the 
    get/post requests from the user.



Features implemented with varying degrees of functionality:

User accounts with login, email verification, basic user account into

Ability to post properties with info, pics, ect.

Marketplace for browsing properties, click either on the map or on the images to see them.

Property pages with info, and the ability to contact the property poster.

User account pages with messaging, and activity feed, and scheduling. You should be able to edit props here as well.


You'll find bugs in the app--it's unfinished, assume you were right and I goofed.

Contact me at michael.skarlinski@gmail.com